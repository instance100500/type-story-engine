import { StoryError } from "./StoryError";
import { StorySceneConstructor } from "./storyScene";
import { nameOf } from "./util";

export class StoryState {
    sceneClassName: string;
    statusSceneClassName: string;
    returningSceneName: string;

    constructor(initialSceneConstructor: StorySceneConstructor) {
        this.sceneClassName = nameOf(initialSceneConstructor);
    }

    validate() {
        if (this.sceneClassName == null || this.sceneClassName.length == 0)
            throw new StoryError('Story state must contain sceneClassName.')
    }
}

export type StoryStateConstructor = { new(): StoryState };