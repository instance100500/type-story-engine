import { StoryError } from "./StoryError";
import { StorySceneMap } from "./storyScene";
import { StoryStateConstructor } from "./storyState";

export class StoryOptions {
    /** This will be used as a prefix for saved game files in local storage
        to prevent accidental overlapping with other story apps. */
    appName: string;

    /** Map: name -> constructor, for all scenes in the story. */
    sceneMap: StorySceneMap;

    /** This function is used to create initial state of the story. */
    stateConstructor: StoryStateConstructor;

    validate() {
        if (!this.sceneMap)
            throw new StoryError('Need options.sceneMap');
        if (!this.appName)
            throw new StoryError('Need options.appPrefix');
        if (!this.stateConstructor)
            throw new StoryError('Need state constructor');
    }
}

