import { StoryRunner } from './storyRunner';
import * as process from 'process';
import * as readline from 'readline';
import fs from 'fs';
import { StoryChoice, StoryElement } from './storyElement';
const cli_colors = require('cli-colors');

export class CommandLineStoryRunner extends StoryRunner {
    readonly appDir = process.cwd();
    readonly localStorageDirName = 'localStorage';
    readonly localStorageDir = this.appDir + '/' + this.localStorageDirName;

    /** @override */
    async readInput(): Promise<string> {
        const reader = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        return new Promise(resolve => {
            reader.question('> ', (answer: string) => {
                resolve(answer);
                reader.close();
            })
        });
    }

    /** @override */
    async loadStateText(fileName: string): Promise<string> {
        const filePath = this.localStorageDir + '/' + this.getScopedFileName(fileName);
        return fs.existsSync(filePath)
            ? fs.readFileSync(filePath).toString()
            : null;
    }

    /** @override */
    async saveStateText(fileName: string, stateAsText: string) {
        if (!fs.existsSync(this.localStorageDir))
            fs.mkdirSync(this.localStorageDir);
        const filePath = this.localStorageDir + '/' + this.getScopedFileName(fileName);
        fs.writeFileSync(filePath, stateAsText);
    }

    /** @override */
    async postMessage(elements: StoryElement[]) {
        for (const element of elements) {
            if (typeof element == 'string')
                console.log(this.reformatText(element));
            if (element instanceof StoryChoice)
                console.log(cli_colors.red(element.id) + ': ' + this.reformatText(element.text));
        }
    }

    reformatText(text: string) {
        let newText = '';
        let previousCharacterIsNewLine = true;
        for (const character of text) {
            if (character == '\r')
                continue;
            if (previousCharacterIsNewLine)
                if (character == ' ' || character == '\t')
                    continue;
            previousCharacterIsNewLine = character == '\n';
            newText += character;
        }
        return newText;
    }
}

