export class StoryElementBase {
    _storyElementBase: void;
}

export class StoryChoice extends StoryElementBase {
    id: string;
    text: string;

    constructor(id: string, text: string) {
        super();
        this.id = id;
        this.text = text;
    }
}

export type StoryElement = StoryElementBase | string;