import { StoryChoice, StoryElement } from "./storyElement";
import { StorySceneNotFoundError } from "./StoryError";
import { StoryOptions } from "./storyOptions";
import { StoryRunnerCommand } from "./storyRunnerCommand";
import { StoryScene, StorySceneConstructor } from "./storyScene";
import { StoryState } from "./storyState";

export class StoryRunner {
    static readonly AUTO_SAVE_FILENAME = 'auto-save';
    static readonly QUICK_SAVE_FILENAME = 'quick-save';
    static readonly SAVE_FILE_EXTENSION = '.json';

    options: StoryOptions;

    constructor(options: StoryOptions) {
        options.validate();
        this.options = options;
    }

    async run() {
        await this.runInner();
    }

    async runInner() {
        let state = await this.loadState(StoryRunner.AUTO_SAVE_FILENAME);
        if (state == null) {
            state = new this.options.stateConstructor;
            state.validate();
        }

        const ctor = this.getRequiredSceneConstructor(state.sceneClassName);
        let scene = this.createSceneWithState(ctor, state);

        while (scene != null) {
            scene = await this.runScene(scene);
        }
    }

    async runScene(scene: StoryScene): Promise<StoryScene> {
        await this.saveState(StoryRunner.AUTO_SAVE_FILENAME, scene.state);
        await this.render(scene);
        let userInput = await this.readInput();
        if (userInput != null)
            userInput = userInput.trim();
        if (userInput == null) {
            await this.postMessage([`Story ended because of lack of input from the user.`])
            return null;
        }
        return await this.processCommand(userInput, scene);
    }

    async processCommand(command: string, scene: StoryScene): Promise<StoryScene> {
        switch (command) {
            case StoryRunnerCommand.status: {
                return this.createStatusScene(scene);
            }
            case StoryRunnerCommand.save: {
                const fileName = StoryRunner.QUICK_SAVE_FILENAME;
                await this.saveState(fileName, scene.state);
                await this.postMessage(['Saved as: ' + fileName]);
                break;
            }
            case StoryRunnerCommand.load: {
                const fileName = StoryRunner.QUICK_SAVE_FILENAME;
                const state = await this.loadState(fileName);
                if (state != null) {
                    const sceneConstructor = this.getRequiredSceneConstructor(state.sceneClassName);
                    const newScene = this.createSceneWithState(sceneConstructor, state);
                    await this.postMessage(['Loaded: ' + fileName]);
                    return newScene;
                } else
                    await this.postMessage(['Cannot load file: ' + fileName]);
                break;
            }
            case StoryRunnerCommand.exit: {
                await this.postMessage([`Story ended by user's request`]);
                return null;
            }
            case StoryRunnerCommand.reset: {
                const initialState = new this.options.stateConstructor;
                const initialSceneConstructor = this.getRequiredSceneConstructor(initialState.sceneClassName);
                const initialScene = this.createSceneWithState(initialSceneConstructor, initialState);
                return initialScene;
            }
            case StoryRunnerCommand.help: {
                await this.postHelpMessage(scene.state);
                break;
            }
            default: {
                const newSceneConstructor = scene.receiveCommand(command);
                if (newSceneConstructor != null) {
                    const newScene = this.createSceneWithState(newSceneConstructor, scene.state);
                    return newScene;
                }
            }
        }
        return scene;
    }

    /** This function must be implemented in a descendant */
    async readInput(): Promise<string> {
        return null;
    }

    async render(scene: StoryScene) {
        const elements = scene.render();
        await this.postMessage(elements);
    }

    /** This function must be implemented in a descendant */
    async postMessage(elements: StoryElement[]) {
        for (const element of elements)
            console.log(element);
    }

    async loadState(fileName: string): Promise<StoryState> {
        const stateAsText = await this.loadStateText(fileName);
        if (stateAsText != null && stateAsText.length > 0) {
            const stateObject = JSON.parse(stateAsText);
            const state = Object.assign(new this.options.stateConstructor, stateObject) as StoryState;
            state.validate();
            return state;
        } else
            return null;
    }

    /** Load state from localStorage.
        This function can be optionally overridden in a descendant to save state to a different type of storage. */
    async loadStateText(fileName: string): Promise<string> {
        return localStorage.getItem(this.getScopedFileName(fileName));
    }

    getScopedFileName(fileName: string) {
        return this.options.appName + '.' + fileName + StoryRunner.SAVE_FILE_EXTENSION;
    }

    async saveState(fileName: string, state: StoryState) {
        const stateAsText = JSON.stringify(state, null, '\t');
        await this.saveStateText(fileName, stateAsText);
    }

    /** Save state to localStorage.
        This function can be optionally overridden in a descendant to provide a different storage for saves. */
    async saveStateText(fileName: string, stateAsText: string) {
        localStorage.setItem(this.getScopedFileName(fileName), stateAsText);
    }

    getSceneConstructor(sceneClassName: string): StorySceneConstructor {
        return this.options.sceneMap[sceneClassName];
    }

    getRequiredSceneConstructor(sceneClassName: string): StorySceneConstructor {
        const ctor = this.getSceneConstructor(sceneClassName);
        if (ctor == null)
            throw new StorySceneNotFoundError('Scene not found: "' + sceneClassName + '"');
        return ctor;
    }

    createSceneWithState(ctor: StorySceneConstructor, state: StoryState): StoryScene {
        const scene = new ctor;
        scene.state = state;
        scene.state.sceneClassName = ctor['name'];
        scene.sceneMap = this.options.sceneMap;
        return scene;
    }

    createStatusScene(currentScene: StoryScene) {
        const returningSceneName = currentScene.state.sceneClassName;
        const statusSceneCtor = this.getRequiredSceneConstructor(currentScene.state.statusSceneClassName);
        const statusScene = this.createSceneWithState(statusSceneCtor, currentScene.state);
        // Avoid being locked into status scene by calling createStatusScene when currentScene is already status scene
        if (!(currentScene instanceof statusSceneCtor))
            statusScene.state.returningSceneName = returningSceneName;
        return statusScene;
    }

    async postHelpMessage(state: StoryState) {
        const helpElements = [
            new StoryChoice(StoryRunnerCommand.help, 'View help'),
            new StoryChoice(StoryRunnerCommand.save, 'Save game'),
            new StoryChoice(StoryRunnerCommand.load, 'Load saved game'),
            new StoryChoice(StoryRunnerCommand.reset, 'Start story from the very beginning')
        ];
        if (state.statusSceneClassName != null)
            helpElements.push(new StoryChoice(StoryRunnerCommand.status, 'View status'));
        this.postMessage(helpElements);
    }
}