export enum StoryRunnerCommand {
    exit = 'exit',
    save = 'save',
    load = 'load',
    status = 'status',
    reset = 'reset',
    help = 'help'
}